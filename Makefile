all : wz_faad.so

wz_faad.so : libs/libfaad/*.c
	gcc -Wall -g -fPIC --shared $^ -o $@ 

test: test_aac.exe

test_aac.exe: test/test_aac.c
	gcc -Wall -g $^ -o $@ 

clean :
	rm *.exe *.so

